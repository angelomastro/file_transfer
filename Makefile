#Gnu compiler options
CXX = g++
CPPFLAGS += -I/usr/local/include -pthread
CXXFLAGS += -std=c++11
LDFLAGS += -L/usr/local/lib `pkg-config --libs grpc++ grpc`       \
           -Wl,--no-as-needed -lgrpc++_reflection -Wl,--as-needed \
           -lprotobuf -lpthread -ldl

#Protocol Buffer and Grpc options
PROTOC = protoc
GRPC_CPP_PLUGIN = grpc_cpp_plugin
GRPC_CPP_PLUGIN_PATH ?= `which $(GRPC_CPP_PLUGIN)`
vpath %.proto .

#make
all: client server

client: filetransfer.pb.o filetransfer.grpc.pb.o client/client.o
	$(CXX) $^ $(LDFLAGS) -o client/$@.tsk

server:  filetransfer.pb.o filetransfer.grpc.pb.o server/server.o
	$(CXX) $^ $(LDFLAGS) -o server/$@.tsk

.PRECIOUS: %.grpc.pb.cc
%.grpc.pb.cc: %.proto
	$(PROTOC) --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN_PATH) $<

.PRECIOUS: %.pb.cc
%.pb.cc: %.proto
	$(PROTOC) --cpp_out=. $<

clean:
	rm -f *.o *.pb.cc *.pb.h client/*.tsk server/*.tsk server/*.o server*.pyc test/*.pyc client/*.o
