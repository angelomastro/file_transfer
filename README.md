# Client-Server using Protobuffers #

This is an example of a client-server request in c++ using the Google protobuffer protocol. This repo aims to simulate a download request for files of three different sizes. The large files will be split (by the server) into chunks and sent sequentially to the client. The latter will later have to put them back together and save the file. A Python unittest makes sure the files downloaded match those transfered by the server. 

### Pre-requisites ###

1. You must install the protobuf package

    Install: https://github.com/google/protobuf/blob/master/src/README.md
    
    Tutorial: https://developers.google.com/protocol-buffers/docs/overview
 
2. You must install gRPC for client server to work

    Install: https://github.com/grpc/grpc/blob/master/INSTALL.md

    Tutorial: http://www.grpc.io/docs/tutorials/basic/c.html

3. Generate your sample test files with python

    Make a file (small.csv) of 3Mb, a medium.csv for 10+ Mb, and a large one (large.csv) 1500+ Mb ... or any size you want

        $ cd server
        $ python datagen.py <output_file> [, <file_size_Mb> = 1]

4. Build the repository with the Makefile

        $ cd ..
        $ make

### Launching the services ###

1. Run the server (on your machine or another one)

        $ cd server
        $ ./server.tsk 

2. Run the client 
    
    If the server isn't running on the same machine, adjust the IP address in the client.cc file and then type

        $ cd client
        $ make client

    If the client will be tested on the same machine keep going with

        $ cd client
        $ ./client.tsk <filename>

    where <filename> = "small.csv", "medium.csv", "large.csv", that the server holds in its repo


### Testing files transferred ###

1. Testing when client/server have been run on the same machine

    The aim of an automated test is to check that the server file exists on the client side and is the same

        $ cd test
        $ python -m unittest test_transfer.TestTransfer.test_small

    If you have generated the other files, you can also run these


        $ python -m unittest test_transfer.TestTransfer.test_medium
        $ python -m unittest test_transfer.TestTransfer.test_large

    Or run all of them

        $ python test_transfer.py

2. Notes on tests

    * It could be quicker to build a python API (python client on gRPC) and use the unittest module on it. 

    * It could be more robust to mock the DataServer class, and check if it sends chunk by chunk the expected pieces of a large file. Same with the DataClient (if it receives).


### Contribution guidelines ###
angelo.mastro@gmail.com