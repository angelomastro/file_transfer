import os.path
import filecmp
import unittest


class TestTransfer(unittest.TestCase):

    smallClient = "../client/small.csv"
    smallServer = "../server/small.csv"

    mediumClient = "../client/medium.csv"
    mediumServer = "../server/medium.csv"

    largeClient = "../client/large.csv"
    largeServer = "../server/large.csv"

    def test_small(self):
        self.assertTrue(os.path.isfile(self.smallClient))
        self.assertEqual(os.path.getsize(self.smallClient), os.path.getsize(self.smallServer))
        self.assertTrue(filecmp.cmp(self.smallClient, self.smallServer))

    def test_medium(self):
        self.assertTrue(os.path.isfile(self.mediumClient))
        self.assertEqual(os.path.getsize(self.mediumClient), os.path.getsize(self.mediumServer))
        self.assertTrue(filecmp.cmp(self.mediumClient, self.mediumServer))

    def test_large(self):
        self.assertTrue(os.path.isfile(self.largeClient))
        self.assertEqual(os.path.getsize(self.largeClient), os.path.getsize(self.largeServer))
        self.assertTrue(filecmp.cmp(self.largeClient, self.largeServer))


if __name__ == '__main__':
    unittest.main()