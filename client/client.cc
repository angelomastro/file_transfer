#include <grpc++/grpc++.h>
#include "../filetransfer.pb.h"
#include "../filetransfer.grpc.pb.h"

#include <fstream>
#include <iostream>
#include <memory>


namespace filetransfer
{

class DataClient
{
public:

    DataClient(std::shared_ptr<grpc::Channel> channel)
                : stub_(DataService::NewStub(channel)) 
    {
    }

    void download(std::string const& fileName)
    {    
        DataRequest request;
        request.set_filename(fileName);
    
        grpc::ClientContext context;
        std::unique_ptr<grpc::ClientReader<DataResponse> > reader(
            stub_->GetData(&context, request));

        DataResponse response;
        while (reader->Read(&response))
        {
            if (!response.error().empty())
            {
                std::cerr << "file download failed: " 
                          << response.error()
                          << std::endl;
                return;
            }

            writeFile(response.filename(), response.chunk(), response.offset());
        }

        grpc::Status status = reader->Finish();

        if (!status.ok())
        {
            std::cerr << status.error_code() << ": " 
                      << status.error_message()
                      << std::endl;
            return;
        }

    }

private:

    void writeFile(std::string const& fileName, 
                   std::string const& content = "",
                   std::streampos offset = 0)
    {
        std::fstream output(fileName,  std::fstream::out | std::fstream::app);
        if (!output.is_open())
        {
            std::cerr << "Error in opening the file " << fileName
                      << " for writing. Failed to parse response" << std::endl;
            return;
        }
        output.seekp(offset);
        output.write(&content[0], content.length());
    }

    std::unique_ptr<DataService::Stub> stub_;
};

}


int main(int argc, char** argv)
{

    filetransfer::DataClient client(
                    grpc::CreateChannel(
                        "localhost:50051", 
                        grpc::InsecureChannelCredentials()));

    if (argc < 2)
    {
        std::cout << "=== client downloader app ===" << std::endl;
        std::cout << "$ ./client.tsk <filename>" << std::endl;
        return 1;
    }

    std::string fileName(argv[1]);

    client.download(fileName);

    return 0;
}