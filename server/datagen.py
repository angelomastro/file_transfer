from datetime import datetime
import os.path
from random import random
import sys


class DataStore(object):

    stocks = ["AI.PA","BARC.L","OR.PA","BNP.PA","RDSA.L","BAYN.DE","LLOY.L",
               "RB.L","DBK.DE","ENI.MI","SU.PA","CSGN.VX","INGA.AS","CFR.VX",
               "MC.PA","ABI.BR","BP.L","BATS.L","SAN.PA","NESN.VX","ALV.DE",
               "IMB.L","NOVN.VX","BT-A.L","PRU.L","DTE.DE","BBVA.MC","ULVR.L",
               "NOVO-B.CO","VOD.L"]

    currency = ["EUR","GBp","EUR","EUR","GBp","EUR","GBp",
                 "GBp","EUR","EUR","EUR","CHF","EUR","CHF",
                 "EUR","EUR","GBp","GBp","EUR","CHF","EUR",
                 "GBp","GBp","GBp","GBp","EUR","EUR","GBp",
                 "EUR","GBp"]

    names = ["Air Liquide SA","Barclays PLC","L'Oreal S.A.","BNP Paribas SA",
              "Royal Dutch Shell plc","Bayer Aktiengesellschaft","Lloyds Banking Group plc",
              "Reckitt Benckiser Group plc","Deutsche Bank AG","Eni S.p.A.",
              "Schneider Electric S.E.","CS GROUP N","ING Groep N.V.","RICHEMONT N",
              "LVMH Moet Hennessy - Louis Vuitton SE","Anheuser-Busch InBev SA/NV",
              "BP p.l.c.","British American Tobacco p.l.c.","Sanofi","NESTLE N",
              "Allianz SE","Imperial Brands PLC","NOVARTIS N","BT Group plc",
              "Prudential plc","Deutsche Telekom AG","Banco Bilbao Vizcaya Argentaria, S.A.",
              "Unilever PLC","Novo Nordisk A/S","Vodafone Group Plc"]

    dlyPrices = [110.85,213.6,187.2,65.71,2119.5,116.8,71.88,7634,17.05,
                  15.09,68.26,14,15.03,81.4,228.2,105.2,474.45,
                  5442,87.69,83,171.15,3701,79.45,314.35,1739,18.01,
                  7.32,4193.5,277.1,225.35]

    dlyPctChg = [0.05,0.05,0.05,0.06,0.07,0.13,0.14,0.18,0.23,0.33,0.35,
                  0.36,0.36,0.37,0.39,0.47,0.49,0.76,0.78,0.79,0.80,0.84,
                  0.89,0.90,0.97,1.21,1.31,1.34,1.73,2.29]

    dlyVolume = [429535,17698321,215890,2214411,2369154,673725,77814253,419061,
                  6133344,8554092,829537,10196127,8805576,960302,217086,603857,
                  16069791,873915,1358385,3924491,773967,664173,3262495,9201540,
                  1940654,6363820,9649358,1168512,2557742,32814748]

    maxTradedVolPct = 10

    traders = ["Joe", "Tom", "Jerry", "Mr Pinkman", "Mr White", "Heisenberg", "Newton"]

    banks = ["Alpha", "Beta", "Beta", "Metha", "Metha", "Eta-Beta", "Epsilon"]
    
    @classmethod
    def generate(cls):
        indexStock = int(random()*len(cls.stocks))
        tradedVol = int(cls.dlyVolume[indexStock]*random()*cls.maxTradedVolPct/100)
        tradedPctChg = -1*cls.dlyPctChg[indexStock] + random()*2*cls.dlyPctChg[indexStock]
        tradedPrice = cls.dlyPrices[indexStock] * (1+tradedPctChg/100)

        indexTrader = [int(random()*len(cls.traders)), int(random()*len(cls.traders))]
        while indexTrader[0] == indexTrader[1]:
            indexTrader[1] = int(random()*len(cls.traders))

        nowUtc = (datetime.utcnow() - datetime(1970,1,1)).total_seconds()
        rndDateTime = random()*nowUtc
        dtUtc = datetime.utcfromtimestamp(rndDateTime)

        #date, time, stock, name, price, cur, %chg, volume, buyer, buyer_bank, seller, seller_bank
        return [dtUtc.strftime("%Y%m%d"),
                dtUtc.strftime("%H:%M:%S.%f"),
                cls.stocks[indexStock], 
                cls.names[indexStock], 
                str(round(tradedPrice,2)), 
                cls.currency[indexStock],
                str(round(tradedPctChg,2)),
                str(tradedVol),
                cls.traders[indexTrader[0]], 
                cls.banks[indexTrader[0]],
                cls.traders[indexTrader[1]], 
                cls.banks[indexTrader[1]]]



if __name__ == "__main__":
    if len(sys.argv) < 2 or sys.argv[1] == "-h" or sys.argv[1] == "--help":
        print "=== financial random data generator ==="
        print "python datagen.py <output_file> [, <file_size_Mb> = 1]"
        sys.exit(1)

    def getLines(num):
        multi = 1000
        names = ["", "ths", "mil", "bil", "ths bil", "mil bil", "bil bil"]
        index = 0
        while num / multi and index < len(names):
            index += 1
            num /= multi
        return "%s %s" %("{:,}".format(num), names[index])

    def getSize(num):
        multi = 1000
        names = ["b", "Kb", "Mb", "Gb", "Tb"]
        index = 0
        while num / multi and index < len(names):
            index += 1
            num /= multi
        return "%s %s" %("{:,}".format(num), names[index])


    minSize = 1000 * 1024
    fileName = sys.argv[1]
    fileSize = int(sys.argv[2])*minSize if len(sys.argv) > 2 else 1*minSize

    counter = [0,0] #size, number of lines
    logLimitBytes = 10*minSize
    with open(fileName, 'w') as output:
        while counter[0] < fileSize:
            data = ",".join(DataStore.generate())
            output.write(data + "\n")
            counter[0] = os.path.getsize(fileName)
            counter[1] += 1
            if counter[0] > logLimitBytes:
                print "... written %s " % getSize(counter[0])
                logLimitBytes *= 2
    print "-> written %s (%s lines, for %s)" % (fileName, 
            getLines(counter[1]), getSize(counter[0]))
    sys.exit(0)
