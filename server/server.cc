#include <grpc++/grpc++.h>
#include "../filetransfer.pb.h"
#include "../filetransfer.grpc.pb.h"

#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>

namespace filetransfer
{

class DataServer final : public DataService::Service
{
private:
    grpc::Status GetData(grpc::ServerContext* context, 
                         const DataRequest* request,
                         grpc::ServerWriter<DataResponse>* writer) override 
    {
        std::cout << "serving GetData (file "  
                  << request->filename() <<")  " << std::endl;

        if (!fileExists(request->filename()))
        {
            DataResponse errMsg;
            errMsg.set_filename(request->filename());
            errMsg.set_error("file not found on the server");
            writer->Write(errMsg);
            std::cerr << "-> file not found " << std::endl;
            return grpc::Status::OK;
        }

        std::streampos size = fileSize(request->filename());
        std::streampos offset = 0;

        size_t chunkId = 1;
        size_t numChunks = size/limitSize + 1;

        std::cout << "-> file to be delivered in " 
                  << numChunks << " chunks" << std::endl;
        while (size > 0)
        {
            DataResponse msg;
            msg.set_filename(request->filename());
            msg.set_chunkid(chunkId);
            msg.set_numberofchunks(numChunks);
            
            std::streampos sizeToDo = size < limitSize ? size : limitSize;
            parseChunk(request->filename(), msg, sizeToDo, offset);

            writer->Write(msg);

            size -= sizeToDo;
            offset += sizeToDo;
            ++chunkId;
        }

        return grpc::Status::OK;
    }

    bool fileExists(std::string const& fileName)
    {
        std::ifstream input(fileName, std::fstream::in | std::ifstream::binary);
        return input.is_open();
    }

    std::streampos fileSize(std::string const& fileName)
    {
        std::ifstream input(fileName, std::fstream::in);
        input.seekg(0, input.end);
        std::streampos size = input.tellg();
        input.seekg(0, input.beg);
        std::cout << fileName << " size: " << sizeInBytes(size) << std::endl;
        return size;
    }

    std::string sizeInBytes(size_t size)
    {
        std::string sizes[5] = {"bytes", "Kb", "Mb", "Gb", "Tb"};
        size_t index = 0;
        while (index < 5 && size / 1000 > 0)
        {
            size /= 1000;
            ++index;
        }
        std::ostringstream os; os << size << sizes[index];
        return os.str();
    }

    void parseChunk(std::string const& fileName,
                    DataResponse& response,
                    std::streampos chunkSize,
                    std::streampos offset = 0)
    {
        std::fstream input(fileName, std::fstream::in | std::fstream::binary);
        if (!input.is_open())
        {
            std::cerr << "Failed to open input file " << fileName 
                      << ". Chunk not sent to client" << std::endl;
            return;
        }
        input.seekg(offset);
        std::string chunk; chunk.resize(chunkSize);
        input.read(&chunk[0], chunkSize);
        response.set_chunk(chunk);
        response.set_offset(offset);
    }

    const std::streampos limitSize = 4000000; //max 4Mb/req 
};

}

void RunServer()
{
    std::string server_address("0.0.0.0:50051");
    filetransfer::DataServer service;

    grpc::ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);
    
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
}

int main(int argc, char** argv)
{
    RunServer();
    return 0;
}
